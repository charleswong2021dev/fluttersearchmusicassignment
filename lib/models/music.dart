class Music {
  final int resultCount;
  List<Details> results;

  Music({required this.resultCount, required this.results});

  factory Music.fromMap(Map<String, dynamic> map) {
    return Music(
        resultCount: map['resultCount'],
        results: map['results']
            .map<Details>((data) => Details.fromMap(data))
            .toList());
  }
}

class Details {
  late String? kind;
  late String? artistName;
  late String? trackName;
  late String? trackViewUrl;
  late String? artworkUrl100;

  Details(
      {required this.kind,
      required this.artistName,
      required this.trackName,
      required this.trackViewUrl,
      required this.artworkUrl100});

  factory Details.fromMap(Map<String, dynamic> map) {
    return Details(
      kind: map['kind'],
      artistName: map['artistName'],
      trackName: map['trackName'],
      trackViewUrl: map['trackViewUrl'],
      artworkUrl100: map['artworkUrl100'],
    );
  }
}
