import 'package:get/get.dart';

class BaseRequest {
  RxBool? loading;

  void setLoading(RxBool loading) {
    this.loading = loading;
  }

  RxBool? getLoading() {
    return loading;
  }
}
