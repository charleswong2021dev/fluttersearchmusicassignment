import 'package:get/get.dart';
import 'package:searchmusic/main/model/music_bar_model.dart';
import 'package:searchmusic/search/Response/music_response.dart';
import 'package:audioplayers/audioplayers.dart';

class MusicBarController extends GetxController {
  var musicObs = Details().obs;
  RxBool isPlay = false.obs;
  var showMusicBarObs = MusicBarModel(false).obs;

  AudioPlayer audio = AudioPlayer();

  initAudioPlayer() {
    isPlay.value = true;
    audio.pause();
    audio = AudioPlayer();
  }

  stopAudio() {
    audio.pause();
  }

  playAudio(String url) async {
    await audio.play(url);
    audio.onPlayerCompletion.listen((event) {
      initAudioPlayer();
    });
  }
}
