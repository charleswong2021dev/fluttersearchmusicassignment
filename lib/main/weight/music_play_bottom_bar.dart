import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:searchmusic/main/controller/music_bar_controller.dart';
import 'package:searchmusic/main/model/music_bar_model.dart';

class MusicPlayBottomBar extends StatefulWidget {
  const MusicPlayBottomBar({Key? key}) : super(key: key);

  @override
  State<MusicPlayBottomBar> createState() => _MusicPlayBottomBar();
}

MusicBarController mainController = Get.find<MusicBarController>();

class _MusicPlayBottomBar extends State<MusicPlayBottomBar> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
        color: Colors.deepPurpleAccent,
        width: double.infinity,
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          //change here don't //worked
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                width: 60.0,
                height: 60.0,
                child:
                    Image.network(mainController.musicObs.value.artworkUrl100)),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: RichText(
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            strutStyle: StrutStyle(fontSize: 12.0),
                            text: TextSpan(
                                style: TextStyle(color: Colors.black),
                                text: mainController.musicObs.value.trackName),
                          ),
                        )
                      ],
                    ),
                  ),
                  Flexible(
                      child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(mainController.musicObs.value.artistName,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 9.0,
                              ))
                        ]),
                  ))
                ],
              ),
            ),
            new Spacer(),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                    icon: mainController.isPlay.value
                        ? new Icon(Icons.play_arrow)
                        : new Icon(Icons.stop),
                    highlightColor: Colors.pink,
                    onPressed: () {
                      setState(() {
                        if (mainController.isPlay.value) {
                          mainController.isPlay.value = false;
                          mainController.playAudio(
                              mainController.musicObs.value.previewUrl);
                        } else {
                          mainController.isPlay.value = true;
                          mainController.stopAudio();
                        }
                      });
                    }),
                IconButton(
                  icon: new Icon(Icons.cancel),
                  highlightColor: Colors.pink,
                  onPressed: () {
                    mainController.stopAudio();
                    mainController.showMusicBarObs.value = MusicBarModel(false);
                  },
                ),
              ],
            )
            // This Icon
          ],
        )));
  }
}
