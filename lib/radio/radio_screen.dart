import 'package:flutter/cupertino.dart';
import 'package:searchmusic/common/common_widget.dart';
import 'package:searchmusic/radio/model/hot_radio_model.dart';
import 'package:searchmusic/radio/radio_widget.dart';

List<HotRadioModel> dummyData = [
  //https://is1-ssl.mzstatic.com/image/thumb/Podcasts115/v4/dc/1f/7f/dc1f7fd4-32a6-b4b0-fed7-bd7ce6dd2beb/mza_16303272747632671479.jpg/600x600bb.jpg
  HotRadioModel(
      "Apple Music 1",
      "Real talk with Jacks",
      "Live-17:00-19:00",
      "Jackson Johnson Podcast",
      "Health, Flthess, Lifestyle, Mindest, Business",
      "https://is1-ssl.mzstatic.com/image/thumb/Podcasts115/v4/dc/1f/7f/dc1f7fd4-32a6-b4b0-fed7-bd7ce6dd2beb/mza_16303272747632671479.jpg/600x600bb.jpg",
      "https://podcasts.apple.com/us/podcast/real-talk-with-jacks/id1435684516?uo=4"),
  HotRadioModel(
      "Apple Music Hits",
      "Trail Mix Podcast with Jack Johnson",
      "Live-20:00-21:00",
      "Jack Johnson",
      "Trail Mix Podcast with Jack Johnson",
      "https://is2-ssl.mzstatic.com/image/thumb/Podcasts115/v4/fd/f0/d3/fdf0d3ee-b772-6e5f-1eef-776f81ef8138/mza_11349691271811519565.jpg/600x600bb.jpg",
      "https://podcasts.apple.com/us/podcast/trail-mix-podcast-with-jack-johnson/id1534528784?uo=4")
];

class RadioScreen extends StatefulWidget {
  const RadioScreen({Key? key}) : super(key: key);

  @override
  State<RadioScreen> createState() => _RadioScreen();
}

class _RadioScreen extends State<RadioScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(
      children: [
        TopBar("Radio"),
        Flexible(
            child: ListView.builder(
                itemCount: dummyData.length,
                itemBuilder: (BuildContext context, int index) {
                  return RadioItemCard(
                    model: dummyData[index],
                  );
                }))
      ],
    ));
  }
}
