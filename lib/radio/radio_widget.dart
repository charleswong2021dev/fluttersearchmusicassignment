import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:searchmusic/models/music.dart';
import 'package:get/get.dart';
import 'package:searchmusic/screen/webview_page.dart';
import 'model/hot_radio_model.dart';

class RadioItemCard extends StatelessWidget {
  final HotRadioModel model;

  // late Details details;
  //
  RadioItemCard({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: Container(
            child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 5, top: 8, bottom: 1),
                    child: Text(model.title,
                        style: TextStyle(color: Colors.black, fontSize: 16))),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 5, top: 1, bottom: 5),
                    child: Text(model.trackName,
                        style: TextStyle(color: Colors.grey, fontSize: 11))),
              ],
            ),
            SizedBox(
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(9.0),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Image.network(model.artworkUrl600),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: double.infinity,
                            height: 100,
                            color: Color(0x8c8c8c).withOpacity(0.5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          top: 13,
                                          right: 10,
                                          bottom: 14),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            model.time,
                                            style: TextStyle(
                                                color: Colors.white54,
                                                fontSize: 10),
                                          ),
                                          Text(
                                            model.radioTitle,
                                            style: TextStyle(
                                                color: Colors.white70,
                                                fontSize: 14),
                                          ),
                                          Text(model.description,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.white54,
                                                  fontSize: 14))
                                        ],
                                      )),
                                ),
                                Stack(
                                  alignment: Alignment.centerRight,
                                  children: [
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Padding(
                                          padding: EdgeInsets.only(right: 10),
                                          child: IconButton(
                                              onPressed: () => {
                                                    Get.to(WebViewPage(
                                                        trackViewUrl:
                                                            model.trackViewUrl,
                                                        trackName:
                                                            model.trackName))
                                                  },
                                              icon: Icon(
                                                Icons.play_circle_outline_sharp,
                                                size: 35,
                                              ))),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ))),
          ],
        )));
  }
}
