class HotRadioModel {
  HotRadioModel(this.title, this.trackName, this.time, this.radioTitle,
      this.description, this.artworkUrl600, this.trackViewUrl);

  late String title;
  late String trackName;
  late String time;
  late String radioTitle;
  late String description;
  late String artworkUrl600;
  late String trackViewUrl;
}
