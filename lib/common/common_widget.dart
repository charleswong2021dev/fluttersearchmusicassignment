import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget TopBar(String title) {
  return Container(
      child: Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 14, top: 8),
            child: Text(
              title,
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ),
        ],
      ),
      Padding(
          padding: EdgeInsets.only(left: 10, top: 2, right: 10),
          child: Container(
            height: 0.2,
            width: double.infinity,
            color: Colors.grey,
          ))
    ],
  ));
}

Widget CommonItemCard(String name) {
  return Container(
      child: Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 14, top: 10, bottom: 5),
            child: Text(
              name,
              style: TextStyle(color: Colors.red, fontSize: 16),
            ),
          ),
        ],
      ),
      Padding(
          padding: EdgeInsets.only(left: 10, top: 2, right: 10),
          child: Container(
            height: 0.2,
            width: double.infinity,
            color: Colors.grey,
          ))
    ],
  ));
}
