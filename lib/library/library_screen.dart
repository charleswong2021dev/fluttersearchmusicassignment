import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:searchmusic/common/common_widget.dart';
import 'package:searchmusic/library/model/music_search_model.dart';
import 'libray_weight.dart';

List<String> dummyItem = [
  "Playlist",
  "Artists",
  "Albums",
  "Songs",
  "TV & Movies"
];
List<MusicSearchModel> dummyMusicSearchData = [
  MusicSearchModel(
      "https://is1-ssl.mzstatic.com/image/thumb/Music115/v4/ae/4c/d4/ae4cd42a-80a9-d950-16f5-36f01a9e1881/source/100x100bb.jpg",
      "Upside Down",
      "https://music.apple.com/us/album/upside-down/1469577723?i=1469577741&uo=4",
      "Jack Johnson"),
  MusicSearchModel(
      "https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/68/ca/d4/68cad45e-e7b7-e8f9-067e-cef6d8fb2f04/source/100x100bb.jpg",
      "A Pirate Looks At Forty",
      "https://music.apple.com/us/album/a-pirate-looks-at-forty-live/1440752312?i=1440752676&uo=4",
      "Jack Johnson"),
  MusicSearchModel(
      "https://is1-ssl.mzstatic.com/image/thumb/Music124/v4/6e/27/6b/6e276b76-8b2c-6a67-e40f-c2b61ba6e52a/source/100x100bb.jpg",
      "Jack Johnson",
      "https://music.apple.com/us/album/jack-johnson/263301268?i=263301273&uo=4",
      "This Bike Is a Pipe Bomb"),
];

class LibraryScreen extends StatefulWidget {
  const LibraryScreen({Key? key}) : super(key: key);

  @override
  State<LibraryScreen> createState() => _LibraryScreen();
}

class _LibraryScreen extends State<LibraryScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        TopBar("Library"),
        Flexible(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: dummyItem.length,
                itemBuilder: (BuildContext context, int index) {
                  return CommonItemCard(dummyItem[index]);
                })),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 14, top: 10, bottom: 5),
              child: Text(
                "Recently Added",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
        Expanded(
          flex: 1,
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: dummyMusicSearchData.length,
              itemBuilder: (BuildContext context, int index) {
                if ((index % 2 == 0) &&
                    index + 1 < dummyMusicSearchData.length) {
                  return CustomTwoGrid(dummyMusicSearchData[index],
                      dummyMusicSearchData[index + 1]);
                } else if (index + 1 == dummyMusicSearchData.length) {
                  return CustomTwoGrid(dummyMusicSearchData[index], null);
                } else {
                  return Container();
                }
              }),
        ),
      ],
    ));
  }
}
