class MusicSearchModel {
  MusicSearchModel(
      this.artworkUrl100, this.trackName, this.trackViewUrl, this.artistName);

  late String artworkUrl100;
  late String trackName;
  late String artistName;
  late String trackViewUrl;
}
