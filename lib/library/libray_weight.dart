import 'package:flutter/material.dart';
import 'package:searchmusic/library/model/music_search_model.dart';

Widget CustomTwoGrid(MusicSearchModel firstModel, MusicSearchModel? secModel) {
  return Padding(
    padding: EdgeInsets.all(5),
    child: Row(
      children: <Widget>[
        Expanded(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Card(
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                    child: Image.network(
                      firstModel.artworkUrl100,
                      fit: BoxFit.cover,
                    )),
                Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Text(
                      firstModel.trackName,
                      style: TextStyle(fontSize: 10, color: Colors.black),
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: Text(firstModel.artistName,
                      style: TextStyle(fontSize: 10, color: Colors.black54)),
                )
              ],
            ),
          ),
        ),
        secModel != null
            ? Expanded(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Card(
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(3.0),
                          ),
                          child: Image.network(
                            secModel.artworkUrl100,
                            fit: BoxFit.cover,
                          )),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                        child: Text(secModel.trackName,
                            style:
                                TextStyle(fontSize: 10, color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                        child: Text(secModel.artistName,
                            style:
                                TextStyle(fontSize: 10, color: Colors.black54)),
                      )
                    ],
                  ),
                ),
              )
            : Expanded(
                child: Container(),
              )
      ],
    ),
  );
}
