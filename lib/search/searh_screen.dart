import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:searchmusic/main/controller/music_bar_controller.dart';
import 'package:searchmusic/main/model/music_bar_model.dart';
import 'package:searchmusic/search/Response/music_response.dart';
import 'package:searchmusic/search/weight/search_weight.dart';

import 'controllers/search_controller.dart';

class SearchScreen extends StatelessWidget {
  final SearchController conroller = Get.put(SearchController());
  final TextEditingController editingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Column(children: [
      SearchBar(editingController, conroller),
      Obx(() => conroller.mLoading.value?  Padding(padding: EdgeInsets.only(top: 10),child: Container(child: CircularProgressIndicator(),)) : Flexible(
          child: conroller.musicResponseObs.value.results != null ? ListView.builder(
              itemCount: conroller.musicResponseObs.value.resultCount,
              itemBuilder: (BuildContext context, int index) {
                return ItemCard(
                    index: index,
                    details: conroller.musicResponseObs.value.results![index],
                );
              }): Container()) )

    ],));

  }
}

class ItemCard extends StatelessWidget {
  final int index;
  late Details details;

  ItemCard(
      {Key? key,
      required this.index,
      required this.details
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    MusicBarController mainController = Get.find<MusicBarController>();
    return GestureDetector(
        onTap: () => {
          mainController.musicObs.value = details,
          mainController.showMusicBarObs.value = MusicBarModel(true)
            },
        child: Container(
            color: Colors.white,
            width: double.maxFinite,
            child: Row(
              children: [
                SizedBox(
                    width: 60.0,
                    height: 60.0,
                    child: Image.network(details.artworkUrl100)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            overflow: TextOverflow.ellipsis,
                            strutStyle: StrutStyle(fontSize: 12.0),
                            text: TextSpan(
                                style: TextStyle(color: Colors.black),
                                text: '${details.trackName}'),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text("${details.artistName}",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 9.0, // insert your font size here
                                ))
                          ]),
                    )
                  ],
                )
              ],
            )));
  }
}
