import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:searchmusic/search/controllers/search_controller.dart';

Widget SearchBar(TextEditingController textEditingController,
    SearchController searchController) {
  return Container(
      color: Colors.grey,
      child: Padding(
          padding: const EdgeInsets.fromLTRB(5.0, 4.0, 5.0, 4.0),
          child: Container(
              height: 40.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(32),
              ),
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                textAlign: TextAlign.left,
                controller: textEditingController,
                decoration: const InputDecoration(
                    hintStyle: TextStyle(fontSize: 15),
                    hintText: 'Search',
                    prefixIcon: Icon(Icons.search),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(bottom: 8.0)),
                onSubmitted: (value) {
                  searchController.fetchMusic(value);
                },
              ))));
}
