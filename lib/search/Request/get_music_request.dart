import 'package:searchmusic/base/base_request.dart';

class GetMusicRequest extends BaseRequest {
  String term;
  String entity;

  GetMusicRequest({required this.term, required this.entity}) : super();

  Map<String, dynamic> getQueryParameters() {
    Map<String, dynamic> params = {"term": term, "entity": entity};
    return params;
  }
}
