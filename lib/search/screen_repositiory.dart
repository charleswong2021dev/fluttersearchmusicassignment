import 'package:searchmusic/base/base_request.dart';
import 'package:searchmusic/network/network_status.dart';

import 'package:searchmusic/network/http_searvice.dart';
import 'package:dio/dio.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:searchmusic/network/api_service.dart';

import 'Request/get_music_request.dart';

class SearchRepository extends APIService {
  HttpService _mHttpService = HttpService();
  RxBool? _loading;
  final _cancelToken = CancelToken();

  SearchRepository();

  Future<NetworkStatus> getMusic(GetMusicRequest request) async {
    NetworkStatus status = await _getRequest(
        getMusicEndPoint(), request.getQueryParameters(), request, true);
    return status;
  }

  Future<NetworkStatus> _getRequest(
      String endPoint,
      Map<String, dynamic> params,
      BaseRequest request,
      bool enableDecode) async {
    _loading = request.loading;
    return await _mHttpService.getRequest(
        endPoint, params, _cancelToken, enableDecode,
        isLoading: _loading);
  }
}
