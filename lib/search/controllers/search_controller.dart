import 'dart:convert';

import 'package:get/get.dart';
import 'package:searchmusic/models/music.dart';
import 'package:searchmusic/models/music_model.dart';
import 'package:searchmusic/network/network_status.dart';
import 'package:searchmusic/network/api_service.dart';
import 'package:searchmusic/search/Request/get_music_request.dart';
import 'package:searchmusic/search/Response/music_response.dart';

import '../screen_repositiory.dart';

class SearchController extends GetxController {
  RxBool mLoading = false.obs;
  late SearchRepository searchRepository;
  var musicResponseObs = MusicResponse().obs;

  @override
  void onInit() {
    searchRepository = SearchRepository();
    super.onInit();
  }

  void fetchMusic(String text) async {
    var request = GetMusicRequest(term: text, entity: "song");
    request.setLoading(mLoading);
    var status = await searchRepository.getMusic(request);
    switch (status.type.runtimeType) {
      case Success:
        print("this is  Success");
        try {
          print("YoutubeMusicResponse success");
          print(status.type.response.data);
          MusicResponse musicResponse;
          musicResponse = MusicResponse.fromJson(status.type.response.data);
          musicResponseObs.value = musicResponse;
          // musicResponseObs.value.results.forEach((element) {
          //   print(element.artistName);
          // });
          print(mLoading.value);
        } on Exception catch (e) {}
        break;
      case Failure:
        print("this is  Failure");
        break;
    }
  }
}
