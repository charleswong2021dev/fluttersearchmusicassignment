// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MusicResponse _$MusicResponseFromJson(Map<String, dynamic> json) =>
    MusicResponse()
      ..resultCount = json['resultCount'] as int?
      ..results = (json['results'] as List<dynamic>?)
          ?.map((e) => Details.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$MusicResponseToJson(MusicResponse instance) =>
    <String, dynamic>{
      'resultCount': instance.resultCount,
      'results': instance.results,
    };

Details _$DetailsFromJson(Map<String, dynamic> json) => Details()
  ..wrapperType = json['wrapperType'] as String
  ..kind = json['kind'] as String
  ..artistId = json['artistId'] as int
  ..collectionId = json['collectionId'] as int
  ..trackId = json['trackId'] as int
  ..artistName = json['artistName'] as String
  ..collectionName = json['collectionName'] as String
  ..trackName = json['trackName'] as String
  ..collectionCensoredName = json['collectionCensoredName'] as String
  ..trackCensoredName = json['trackCensoredName'] as String
  ..artistViewUrl = json['artistViewUrl'] as String
  ..collectionViewUrl = json['collectionViewUrl'] as String
  ..trackViewUrl = json['trackViewUrl'] as String
  ..previewUrl = json['previewUrl'] as String
  ..artworkUrl100 = json['artworkUrl100'] as String;

Map<String, dynamic> _$DetailsToJson(Details instance) => <String, dynamic>{
      'wrapperType': instance.wrapperType,
      'kind': instance.kind,
      'artistId': instance.artistId,
      'collectionId': instance.collectionId,
      'trackId': instance.trackId,
      'artistName': instance.artistName,
      'collectionName': instance.collectionName,
      'trackName': instance.trackName,
      'collectionCensoredName': instance.collectionCensoredName,
      'trackCensoredName': instance.trackCensoredName,
      'artistViewUrl': instance.artistViewUrl,
      'collectionViewUrl': instance.collectionViewUrl,
      'trackViewUrl': instance.trackViewUrl,
      'previewUrl': instance.previewUrl,
      'artworkUrl100': instance.artworkUrl100,
    };
