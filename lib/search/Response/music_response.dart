import 'package:json_annotation/json_annotation.dart';
import 'package:searchmusic/base/base_response.dart';

part 'music_response.g.dart';

@JsonSerializable()
class MusicResponse extends BaseResponse {
  MusicResponse();

  @JsonKey(name: "resultCount")
  int? resultCount;
  @JsonKey(name: "results")
  List<Details>? results;

  factory MusicResponse.fromJson(Map<String, dynamic> json) =>
      _$MusicResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MusicResponseToJson(this);
}

@JsonSerializable()
class Details {
  Details();

  @JsonKey(name: "wrapperType")
  late String wrapperType;
  @JsonKey(name: "kind")
  late String kind;
  @JsonKey(name: "artistId")
  late int artistId;
  @JsonKey(name: "collectionId")
  late int collectionId;
  @JsonKey(name: "trackId")
  late int trackId;
  @JsonKey(name: "artistName")
  late String artistName;
  @JsonKey(name: "collectionName")
  late String collectionName;
  @JsonKey(name: "trackName")
  late String trackName;
  @JsonKey(name: "collectionCensoredName")
  late String collectionCensoredName;
  @JsonKey(name: "trackCensoredName")
  late String trackCensoredName;
  @JsonKey(name: "artistViewUrl")
  late String artistViewUrl;
  @JsonKey(name: "collectionViewUrl")
  late String collectionViewUrl;
  @JsonKey(name: "trackViewUrl")
  late String trackViewUrl;
  @JsonKey(name: "previewUrl")
  late String previewUrl;
  @JsonKey(name: "artworkUrl100")
  late String artworkUrl100;

  factory Details.fromJson(Map<String, dynamic> json) =>
      _$DetailsFromJson(json);

  Map<String, dynamic> toJson() => _$DetailsToJson(this);
}
