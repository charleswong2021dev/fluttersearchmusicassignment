import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class WebViewPage extends StatefulWidget {
  late String trackViewUrl;
  late String trackName;

  WebViewPage({Key? key, required this.trackViewUrl, required this.trackName})
      : super(key: key);

  @override
  WebViewPageState createState() =>
      WebViewPageState(trackViewUrl: trackViewUrl, trackName: trackName);
}

class WebViewPageState extends State<WebViewPage> {
  late String trackViewUrl;
  late String trackName;

  WebViewPageState({required this.trackViewUrl, required this.trackName});

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("$trackName"),
        ),
        body: WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: '${trackViewUrl}'));
  }
}
