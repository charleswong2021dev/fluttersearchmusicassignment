import 'package:dio/dio.dart';

class NetworkStatus {
  late dynamic type;

  NetworkStatus({required this.type});
}

class Success {
  late Response response;

  Success({required this.response});
}

class Failure {
  String errorMessage = "";
  String errorCode = "";

  Failure({required this.errorMessage, required this.errorCode});
}

class Loading {
  bool isLoading = false;

  Loading({required this.isLoading});
}
