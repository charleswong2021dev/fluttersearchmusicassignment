import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:searchmusic/search/Response/music_response.dart';
import '../app_config.dart';
import 'network_status.dart';

class HttpService {
  late Dio _dio;

  //final baseUrl = "https://reqres.in/";
  HttpService() {
    _dio = Dio(BaseOptions(
      baseUrl: BASE_URL,
      connectTimeout: 5000,
      receiveTimeout: 3000,
    ));
    initializeInterceptors();
  }

  int countApi = 0;

  Future<NetworkStatus> getRequest(String endPoint, Map<String, dynamic> params,
      CancelToken cancelToken, bool enableDecode,
      {RxBool? isLoading = null, String? token = null}) async {
    Response response;

    if (isLoading != null) {
      countApi++;
    }
    try {
      print(
          "(start) >> end point:$endPoint, count api:${countApi}, can disable loading:${disableLoading()}");
      isLoading?.value = true;
      if (countApi > 0) {
        countApi--;
      }
      _dio.options.headers['content-Type'] = 'application/json';
      if (token != null) {
        _dio.options.headers["authorization"] = "token ${token}";
      }

      response = await _dio.get(endPoint,
          queryParameters: params, cancelToken: cancelToken);
      if (response.statusCode == 200) {
        if (disableLoading()) {
          isLoading?.value = false;
        }

        //print("NetworkStatus Success :${response.data}");
        print(
            "(end)success >> end point:$endPoint, count api:${countApi}, can disable loading:${disableLoading()}");
        // MusicResponse musicResponse;
        // print(response.data);
        if (enableDecode) {
          var jsonData = json.decode(response.data);
          response.data = jsonData;
        }

        return NetworkStatus(type: Success(response: response));
      }
      print(
          "(end)failure 1 >> end point:$endPoint, count api:${countApi}, can disable loading:${disableLoading()}");
      if (disableLoading()) {
        isLoading?.value = false;
      }
      return NetworkStatus(
          type: Failure(
              errorCode: "${response.statusCode}",
              errorMessage: "connection fail"));
      print("try " + endPoint);
    } on DioError catch (e) {
      if (countApi > 0) {
        countApi--;
      }
      if (disableLoading()) {
        isLoading?.value = false;
      }
      print(
          "(end)failure 2 >> end point:$endPoint, count api:${countApi}, can disable loading:${disableLoading()}");
      return NetworkStatus(
          type: Failure(errorCode: "404", errorMessage: "network_error"));
    }
  }

  initializeInterceptors() {
    _dio.interceptors.add(InterceptorsWrapper(
      onError: (error, handler) {
        // print("onError:"+error.message);
        handler.next(error);
      },
      onRequest: (request, handler) {
        //print("onRequest:${request.method} ${request.path}");
        handler.next(request);
      },
      onResponse: (response, handler) {
        // print(response.data);
        handler.next(response);
      },
    ));
  }

  bool disableLoading() {
    if (countApi <= 0) {
      return true;
    }
    return false;
  }
}
