import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:searchmusic/models/music.dart';

abstract class APIService {
  String getMusicEndPoint() {
    return "/search";
  }
}
