import 'package:flutter/material.dart';
import 'package:searchmusic/library/library_screen.dart';
import 'package:get/get.dart';
import 'package:searchmusic/radio/radio_screen.dart';
import 'main/controller/music_bar_controller.dart';
import 'main/weight/music_play_bottom_bar.dart';
import 'search/searh_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

MusicBarController musicBarController = Get.put(MusicBarController());

class _MyHomePageState extends State<MyHomePage> {
  int currentIndex = 0;
  final screens = [LibraryScreen(), RadioScreen(), SearchScreen()];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: Stack(
        children: [
          Positioned(
            child: screens[currentIndex],
          ),
          Positioned(
              child: Align(
                  alignment: FractionalOffset.bottomLeft,
                  child: Obx(() =>
                      musicBarController.showMusicBarObs.value.showBar
                          ? getMusicPlayBottomBar()
                          : Container()))),
        ],
      )),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.black87,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white70,
        iconSize: 25,
        selectedFontSize: 15,
        unselectedFontSize: 16,
        showSelectedLabels: true,
        showUnselectedLabels: false,
        currentIndex: currentIndex,
        onTap: (index) => {
          setState(() => {currentIndex = index})
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.library_books),
            label: 'Library',
            backgroundColor: Colors.black87,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.radio),
            label: 'Radio',
            backgroundColor: Colors.black87,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
            backgroundColor: Colors.black87,
          ),
        ],
      ),
    );
  }

  MusicPlayBottomBar getMusicPlayBottomBar() {
    musicBarController.initAudioPlayer();
    return MusicPlayBottomBar();
  }
}
